# Holiday

Java Application for helping people to choose where to go on holiday.     
   
While using this app you can:  
  
  - get the information for a certain location    
  - get top 5 locations where you could go on a period A-B of time  
  - where is cheaper to practice 10 days an activity.  
  
This application is based on a Client - Server communication using Sockets.
This communication is used to send commands from GUI (using a code method for every type of query used in the database) to Client.      
From Client, the query is sent to Server which is connected to DataBase, running the query and sending back the answer to Client.        
Now, the answer is displayed to user. :)    
       
Database is a relational type having 5 tables: Country, County, City, Location, Activity and a jonction table between Location and Activity.
   
Application made in IntelIJ IDEA.
      
� Enica Diana - Maria 2018