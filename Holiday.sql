-- MySQL dump 10.13  Distrib 8.0.11, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: vacanta
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activitati`
--

DROP TABLE IF EXISTS `activitati`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `activitati` (
  `idActivitate` int(11) NOT NULL AUTO_INCREMENT,
  `numeActivitate` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idActivitate`),
  UNIQUE KEY `idActivitate_UNIQUE` (`idActivitate`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activitati`
--

LOCK TABLES `activitati` WRITE;
/*!40000 ALTER TABLE `activitati` DISABLE KEYS */;
INSERT INTO `activitati` VALUES (1,'Trasee montane'),(2,'Inot'),(3,'Plaja'),(4,'Plimbare'),(5,'Jogging');
/*!40000 ALTER TABLE `activitati` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `judete`
--

DROP TABLE IF EXISTS `judete`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `judete` (
  `idJudet` int(11) NOT NULL AUTO_INCREMENT,
  `numeJudet` varchar(45) DEFAULT NULL,
  `idTara` int(11) DEFAULT NULL,
  PRIMARY KEY (`idJudet`),
  UNIQUE KEY `idJudete_UNIQUE` (`idJudet`),
  KEY `idTara_idx` (`idTara`),
  CONSTRAINT `idTara` FOREIGN KEY (`idTara`) REFERENCES `tari` (`idtara`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `judete`
--

LOCK TABLES `judete` WRITE;
/*!40000 ALTER TABLE `judete` DISABLE KEYS */;
INSERT INTO `judete` VALUES (1,'Constanta',1),(2,'Bucuresti',1),(3,'Brasov',1);
/*!40000 ALTER TABLE `judete` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locatii`
--

DROP TABLE IF EXISTS `locatii`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `locatii` (
  `idLocatie` int(11) NOT NULL AUTO_INCREMENT,
  `numeLocatie` varchar(45) DEFAULT NULL,
  `pret` int(11) DEFAULT NULL,
  `datastart` varchar(45) DEFAULT NULL,
  `dataend` varchar(45) DEFAULT NULL,
  `idOras` int(11) DEFAULT NULL,
  PRIMARY KEY (`idLocatie`),
  UNIQUE KEY `idLocatie_UNIQUE` (`idLocatie`),
  KEY `idOras_idx` (`idOras`),
  CONSTRAINT `idOras` FOREIGN KEY (`idOras`) REFERENCES `orase` (`idoras`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locatii`
--

LOCK TABLES `locatii` WRITE;
/*!40000 ALTER TABLE `locatii` DISABLE KEYS */;
INSERT INTO `locatii` VALUES (1,'Hotel Aurora',301,'20190501','20190915',2),(2,'Hotel Richmond',816,'20190501','20190915',2),(3,'Hotel Flora',275,'20190501','20190915',2),(4,'Hotel Marea Neagra',315,'20190501','20190915',2),(5,'Hotel Iaki',600,'20190501','20190915',2),(6,'Hotel Riviera',400,'20190501','20190915',2),(7,'Hotel Zenith',499,'20190501','20190915',2),(8,'Hotel Vega',911,'20190501','20190915',2),(9,'Hotel Splendid',359,'20190501','20190915',2),(10,'Hotel Ambasador',519,'20190501','20190915',2),(11,'Hotel Ibis',224,'20190501','20190915',1),(12,'Hotel Pacific',280,'20190501','20190915',1),(13,'Hotel Traian',218,'20190501','20190915',1),(14,'Hotel Florentina',180,'20190501','20190915',1),(15,'Hotel Havana',256,'20190501','20190915',1),(16,'Hotel Intercontinental',752,'20190101','20191231',4),(17,'RIN Grand Hotel',235,'20190101','20191231',4),(18,'Hotel Parliament',264,'20190101','20191231',4),(19,'Hotel Venezia',160,'20190101','20191231',4),(20,'Hotel Grand',242,'20190501','20190915',3),(21,'Pensiunea Casa Soarelui',151,'20190501','20190915',3),(22,'Complex La Perla',186,'20190501','20190915',3),(23,'Hotel Valul Magic',135,'20190501','20190915',3),(24,'Hotel Diana',412,'20190501','20190915',3),(25,'Hotel Evia',264,'20190501','20190915',3);
/*!40000 ALTER TABLE `locatii` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orase`
--

DROP TABLE IF EXISTS `orase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `orase` (
  `idOras` int(11) NOT NULL AUTO_INCREMENT,
  `numeOras` varchar(45) DEFAULT NULL,
  `idJudet` int(11) DEFAULT NULL,
  PRIMARY KEY (`idOras`),
  UNIQUE KEY `idOras_UNIQUE` (`idOras`),
  KEY `idJudet_idx` (`idJudet`),
  CONSTRAINT `idJudet` FOREIGN KEY (`idJudet`) REFERENCES `judete` (`idjudet`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orase`
--

LOCK TABLES `orase` WRITE;
/*!40000 ALTER TABLE `orase` DISABLE KEYS */;
INSERT INTO `orase` VALUES (1,'Constanta',1),(2,'Mamaia',1),(3,'Eforie Nord',1),(4,'Bucuresti',2);
/*!40000 ALTER TABLE `orase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `practicare`
--

DROP TABLE IF EXISTS `practicare`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `practicare` (
  `idPracticare` int(11) NOT NULL AUTO_INCREMENT,
  `idLocatie` int(11) DEFAULT NULL,
  `idActivitate` int(11) DEFAULT NULL,
  PRIMARY KEY (`idPracticare`),
  UNIQUE KEY `idPracticare_UNIQUE` (`idPracticare`),
  KEY `idLocatie_idx` (`idLocatie`),
  KEY `idActivitate_idx` (`idActivitate`),
  CONSTRAINT `idActivitate` FOREIGN KEY (`idActivitate`) REFERENCES `activitati` (`idactivitate`),
  CONSTRAINT `idLocatie` FOREIGN KEY (`idLocatie`) REFERENCES `locatii` (`idlocatie`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `practicare`
--

LOCK TABLES `practicare` WRITE;
/*!40000 ALTER TABLE `practicare` DISABLE KEYS */;
INSERT INTO `practicare` VALUES (1,1,2),(2,2,2),(3,3,2),(4,4,2),(5,5,2),(6,6,2),(7,7,2),(8,8,2),(9,9,2),(10,10,2),(11,11,2),(12,12,2),(13,13,2),(14,14,2),(15,15,2),(16,20,2),(17,21,2),(18,22,2),(19,23,2),(20,24,2),(21,25,2),(22,1,3),(23,2,3),(24,3,3),(25,4,3),(26,5,3),(27,6,3),(28,7,3),(29,8,3),(30,9,3),(31,10,3),(32,11,3),(33,12,3),(34,13,3),(35,14,3),(36,15,3),(37,20,3),(38,21,3),(39,22,3),(40,23,3),(41,24,3),(42,25,3),(43,16,4),(44,17,4),(45,18,4),(46,19,4),(47,16,5),(48,17,5),(49,18,5),(50,19,5);
/*!40000 ALTER TABLE `practicare` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tari`
--

DROP TABLE IF EXISTS `tari`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tari` (
  `idTara` int(11) NOT NULL AUTO_INCREMENT,
  `numeTara` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idTara`),
  UNIQUE KEY `idTari_UNIQUE` (`idTara`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tari`
--

LOCK TABLES `tari` WRITE;
/*!40000 ALTER TABLE `tari` DISABLE KEYS */;
INSERT INTO `tari` VALUES (1,'Romania');
/*!40000 ALTER TABLE `tari` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-29 16:42:03
