package GUI;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Menu {
    private JPanel Container;
    private JPanel Top;
    private JPanel Middle;
    private JPanel Bottom;
    private JButton aflaButton1;
    private JButton aflaButton2;
    private JButton aflaButton3;

    public Menu(){
        JFrame frame = new JFrame("Menu");
        frame.setContentPane(Container);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(300,200);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.setVisible(true);

        //listenere pentru cele 3 butoane, fiecare imi deschide un alt form
        aflaButton1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                ViewLocation a = new ViewLocation();
            }
        });
        aflaButton2.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                TopLocations b = new TopLocations();
            }
        });
        aflaButton3.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                CheaperToGo c = new CheaperToGo();
            }
        });
    }
}
