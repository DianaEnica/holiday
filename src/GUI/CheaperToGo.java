package GUI;

import Code.Client;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

public class CheaperToGo {
    private JButton aflaButton;
    private JButton cancelButton;
    private JTextField textField1;
    private JPanel Container;

    public CheaperToGo(){
        JFrame frame = new JFrame("CheaperToGo");
        frame.setContentPane(Container);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(300,200);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.setVisible(true);

        //listener pentru butonul "Afla"
        aflaButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String activitate = textField1.getText();
                String query;
                //formez query-ul sql pe care-l trimit la client impreuna cu tag-ul aferent
                query = "SELECT t.numeTara, j.numeJudet, o.numeOras, l.numeLocatie, l.pret, l.datastart, l.dataend, a.numeActivitate, l.pret * 10 as \"TOTAL\"" +
                        " FROM locatii l join orase o on   (o.idOras = l.idOras)" +
                        " join judete j on (o.idJudet = j.idJudet)" +
                        " join tari t on   (t.idTara = j.idTara)" +
                        " join practicare p on (p.idLocatie = l.idLocatie)" +
                        " join activitati a on (p.idActivitate = a.idActivitate)" +
                        " WHERE a.numeActivitate = '" + activitate + "'"+
                        " ORDER BY TOTAL ASC;";
                Client cl = new Client();
                int tag = 3;
                try {
                    cl.connectServer(tag,query);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });

        //listener pentru butonul "Cancel"
        cancelButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                frame.dispose();
            }
        });

    }
}
