package GUI;

import Code.Client;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

public class ViewLocation {
    private JButton afiseazaButton;
    private JTextField locatieInput;
    private JPanel Container;
    private JButton cancelButton;

    public ViewLocation() {

        JFrame frame = new JFrame("ViewLocation");
        frame.setContentPane(Container);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(300,200);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.setVisible(true);

        //listener pentur butonul Afla
        afiseazaButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String locatie = locatieInput.getText();
                String query;
                //formez query-ul sql
                query = "SELECT t.numeTara, j.numeJudet, o.numeOras, l.numeLocatie, l.pret, l.datastart, l.dataend, a.numeActivitate" +
                        " FROM locatii l join orase o on (o.idOras = l.idOras)" +
                                       " join judete j on (o.idJudet = j.idJudet)" +
                                       " join tari t on (t.idTara = j.idTara)" +
                                       " join practicare p on (p.idLocatie = l.idLocatie)" +
                                        " join activitati a on (p.idActivitate = a.idActivitate)" +
                        " WHERE numeLocatie = '" + locatie + "';";
                //trimit catre Client query + tag-ul aferent operatiei
                Client cl = new Client();
                int tag = 1;
                try {
                    cl.connectServer(tag,query);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });

        //listener pentru butonul Cancel
        cancelButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                frame.dispose();
            }
        });
    }
}
