package GUI;

import Code.Client;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TopLocations {
    private JPanel Container;
    private JButton aflaButton;
    private JButton cancelButton;
    private JRadioButton taraRadioButton;
    private JRadioButton judetRadioButton;
    private JRadioButton orasRadioButton;
    private JTextField locatieInput;
    private JComboBox ziStart;
    private JComboBox lunaStart;
    private JComboBox anStart;
    private JComboBox ziEnd;
    private JComboBox lunaEnd;
    private JComboBox anEnd;

    public int getNumarZile(Date d1, Date d2) {      ///functie care calculeaza numarul de zile dintre 2 date
        int zile = 0;
        long diferenta = d2.getTime() - d1.getTime();
        long difzile = diferenta / (24 * 60 * 60 * 1000) + 1;
        zile = (int) difzile;
        return zile;
    }

    public TopLocations() {
        JFrame frame = new JFrame("TopLocations");
        frame.setContentPane(Container);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(300,200);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.setVisible(true);

        //grupez butoanele de tip Radio pentru a putea fi apasat un singur buton la un moment dat
        ButtonGroup bgroup = new ButtonGroup();
        bgroup.add(taraRadioButton);
        bgroup.add(judetRadioButton);
        bgroup.add(orasRadioButton);

        //listener pentru butonul Afla
        aflaButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                ///transform string in date
                String string1 = ziStart.getSelectedItem() + "-" + lunaStart.getSelectedItem() + "-" + anStart.getSelectedItem();
                String string2 = ziEnd.getSelectedItem() + "-" + lunaEnd.getSelectedItem() + "-" + anEnd.getSelectedItem();
                DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                Date date1 = null;
                Date date2 = null;
                try {
                    date1 = format.parse(string1);
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }
                try {
                    date2 = format.parse(string2);
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }
                int numarZile = getNumarZile(date1,date2);

                ///stringuri ce vor fi folosite in query-uri
                String start = (String) anStart.getSelectedItem()+ lunaStart.getSelectedItem() + ziStart.getSelectedItem();
                String end = (String) anEnd.getSelectedItem()+ lunaEnd.getSelectedItem() + ziEnd.getSelectedItem();

                if(taraRadioButton.isSelected()){           ///TOP 5 locatii din tara
                    String tara = locatieInput.getText();
                    String query;
                    //formez query-ul sql
                    query = "SELECT t.numeTara, j.numeJudet, o.numeOras, l.numeLocatie, l.pret, l.datastart, l.dataend, " +
                            " l.pret * " + numarZile + " as \"TOTAL\" " +
                            " FROM locatii l join orase o on   (o.idOras = l.idOras)" +
                            " join judete j on (o.idJudet = j.idJudet)" +
                            " join tari t on   (t.idTara = j.idTara)" +
                            " WHERE convert(l.dataStart, date) <= convert('" + start + "', date) && " +
                            " convert(l.dataEnd, date) >= convert('" + end + "', date) && t.numeTara = '" + tara +
                            "' ORDER BY TOTAL DESC;";
                    //trimit query catre Client impreuna cu tag-ul aferent
                    Client cl = new Client();
                    int tag = 2;
                    try {
                        cl.connectServer(tag,query);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }else if(judetRadioButton.isSelected()){    ///TOP 5 locatii din judet
                    String judet = locatieInput.getText();
                    String query;
                    //formez query-ul sql
                    query = "SELECT t.numeTara, j.numeJudet, o.numeOras, l.numeLocatie, l.pret, l.datastart, l.dataend, " +
                            " l.pret * " + numarZile + " as \"TOTAL\" " +
                            " FROM locatii l join orase o on   (o.idOras = l.idOras)" +
                            " join judete j on (o.idJudet = j.idJudet)" +
                            " join tari t on   (t.idTara = j.idTara)" +
                            " WHERE convert(l.dataStart, date) <= convert('" + start + "', date) && " +
                            " convert(l.dataEnd, date) >= convert('" + end + "', date) && j.numeJudet = '" + judet +
                            "' ORDER BY TOTAL DESC;";
                    //trimit query catre Client impreuna cu tag-ul aferent
                    Client cl = new Client();
                    int tag = 2;
                    try {
                        cl.connectServer(tag,query);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }else if(orasRadioButton.isSelected()){     ///TOP 5 locatii din oras
                    String oras = locatieInput.getText();
                    String query;
                    //formez query-ul sql
                    query = "SELECT t.numeTara, j.numeJudet, o.numeOras, l.numeLocatie, l.pret, l.datastart, l.dataend, " +
                            " l.pret * " + numarZile + " as \"TOTAL\" " +
                            " FROM locatii l join orase o on   (o.idOras = l.idOras)" +
                            " join judete j on (o.idJudet = j.idJudet)" +
                            " join tari t on   (t.idTara = j.idTara)" +
                            " WHERE convert(l.dataStart, date) <= convert('" + start + "', date) && " +
                            " convert(l.dataEnd, date) >= convert('" + end + "', date) && o.numeOras = '" + oras +
                            "' ORDER BY TOTAL DESC;";
                    //trimit query catre Client impreuna cu tag-ul aferent
                    Client cl = new Client();
                    int tag = 2;
                    try {
                        cl.connectServer(tag,query);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });

        //listener pentru butonul Cancel
        cancelButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                frame.dispose();
            }
        });
    }
}
