package Code;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.*;

public class Server {
    public static void main(String[] args) throws IOException, SQLException, ClassNotFoundException {
        String newline = System.getProperty("line.separator");
        ServerSocket ss;
        Socket cs;
        ss = new ServerSocket(8000);
        System.out.println("Serverul a pornit !");

        //conexiunea la baza de date
        Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/vacanta", "root", "diana");

        //astept query de la client
        int i = 0;
        while (i < 2000) { /// 2000 de conexiuni
            cs = ss.accept();  //astept conexiunea cu clientul
            i++;

            System.out.println("Conexiune efectuata");

            ObjectInputStream dis;
            DataOutputStream dos;
            dis = new ObjectInputStream(cs.getInputStream());
            dos = new DataOutputStream(cs.getOutputStream());

            //am primit query + code
            Stream stream;
            stream = (Stream) dis.readObject();
            String SQLQuery = stream.SQL;
            SQLQuery = SQLQuery.trim();

            String raspunsCatreClient = "";

            ///VIZUALIZEAZA INFORMATII LOCATIA X
            if (stream.code == 1) {
                Statement myState = myConn.createStatement();
                ResultSet myRs = myState.executeQuery(SQLQuery);
                if(myRs.next()){
                    raspunsCatreClient += "Informatii despre locatia cautata: " + newline;
                    raspunsCatreClient += "Tara: " + myRs.getString("numeTara") + newline;
                    raspunsCatreClient += "Judet: " + myRs.getString("numeJudet") + newline;
                    raspunsCatreClient += "Oras: " + myRs.getString("numeOras") + newline;
                    raspunsCatreClient += "Locatie: " + myRs.getString("numeLocatie") + newline;
                    raspunsCatreClient += "Pret: " + myRs.getString("pret") + newline;
                    raspunsCatreClient += "Data Start: " + myRs.getString("datastart") + newline;
                    raspunsCatreClient += "Data End: " + myRs.getString("dataend") + newline;
                    raspunsCatreClient += "Activitati: " + myRs.getString("numeActivitate");
                    while (myRs.next()) {
                        raspunsCatreClient += ", "+ myRs.getString("numeActivitate");
                    }
                } else raspunsCatreClient += "Locatie indisponibila.";

            }
            ///TOP 5 LOCATII UNDE SE POATE MERGE PE PERIOADA A-B
            else if (stream.code == 2) {
                Statement myState = myConn.createStatement();
                ResultSet myRs = myState.executeQuery(SQLQuery);
                boolean amafisat = false;
                int top = 1;
                while(myRs.next() && top<=5){
                    raspunsCatreClient += "Locatia " + top +  newline;
                    raspunsCatreClient += "Tara: " + myRs.getString("numeTara") + " | " ;
                    raspunsCatreClient += "Judet: " + myRs.getString("numeJudet") + " | " ;
                    raspunsCatreClient += "Oras: " + myRs.getString("numeOras") + " | " ;
                    raspunsCatreClient += "Locatie: " + myRs.getString("numeLocatie") + " | " ;
                    raspunsCatreClient += "Pret: " + myRs.getString("pret") + " | " ;
                    raspunsCatreClient += "Data Start: " + myRs.getString("datastart") + " | " ;
                    raspunsCatreClient += "Data End: " + myRs.getString("dataend") + newline;
                    top++;
                    amafisat = true;
                }
                if(!amafisat) raspunsCatreClient += "Top 5 indisponibil.";
            }
            ///CEL MAI IEFTIN SA PRACTIC O ACTIVITATE 10 ZILE
            else if (stream.code == 3) {
                Statement myState = myConn.createStatement();
                ResultSet myRs = myState.executeQuery(SQLQuery);
                if(myRs.next()){
                    raspunsCatreClient += "Locatia " +  newline;
                    raspunsCatreClient += "Tara: " + myRs.getString("numeTara") + newline;
                    raspunsCatreClient += "Judet: " + myRs.getString("numeJudet") + newline;
                    raspunsCatreClient += "Oras: " + myRs.getString("numeOras") + newline;
                    raspunsCatreClient += "Locatie: " + myRs.getString("numeLocatie") + newline;
                    raspunsCatreClient += "Pret: " + myRs.getString("pret") + newline;
                    raspunsCatreClient += "Data Start: " + myRs.getString("datastart") + newline;
                    raspunsCatreClient += "Data End: " + myRs.getString("dataend") + newline;
                    raspunsCatreClient += "Activitate: " + myRs.getString("numeActivitate");
                } else raspunsCatreClient += "Locatie indisponibila.";
            }

            /// RASPUNS CATRE CLIENT
            dos.writeUTF(raspunsCatreClient);
            cs.close();
            dis.close();
            dos.close();
        }
    }
}
