package Code;

import javax.swing.*;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class Client {
    public String connectServer(int code, String SQL) throws IOException {

        //conexiunea la server
        Socket cs = null;
        try {
            cs = new Socket("localhost", 8000);
        } catch (Exception e) {
            System.out.println("Conexiune esuata");
            System.exit(1);
        }

        //comunicarea client-server
        DataInputStream dis;
        ObjectOutputStream dos;
        dis = new DataInputStream(cs.getInputStream());
        dos = new ObjectOutputStream(cs.getOutputStream());

        //trimit query catre server. astept raspuns
        System.out.println("Se trimite QUERY-ul SQL \t");
        System.out.println(SQL);
        SQL = SQL.trim();
        Stream stream = new Stream(code,SQL);
        dos.writeObject(stream); ///QUERY SE TRIMITE LA SERVER

        //serverul s-a conectat la baza de date si trimite raspuns
        String raspunsDeLaServer = dis.readUTF();
        JOptionPane.showMessageDialog(null, raspunsDeLaServer);
        System.out.println("CONEXIUNE CLIENT TERMINATA!");

        return raspunsDeLaServer;
    }
}
